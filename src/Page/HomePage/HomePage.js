import React from "react";
import CarouselMovie from "../../Component/CarouselMovie/CarouselMovie";

import Footer from "../../Component/Footer/Footer";
import ListMovie from "../../Component/ListMovie/ListMovie";
import TabsMovie from "../../Component/TabsMovie/TabsMovie";
import Spinner from "../../Component/Spinner/Spinner";

export default function HomePage() {
  return (
    <div>
      <Spinner />
      <CarouselMovie />
      <ListMovie />
      <TabsMovie />
      <hr />
    </div>
  );
}
